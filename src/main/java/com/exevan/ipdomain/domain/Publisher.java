package com.exevan.ipdomain.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import javax.persistence.OneToMany;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Wouter Dumoulin
 */
@javax.persistence.Entity
//@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Publisher extends Entity implements Serializable {

    private String name;
    @JsonIgnore
    @OneToMany(mappedBy = "publisher") 
    private List<Game> games;

    public Publisher() {
        //super(IdRegistry.PUBLISHERIDREGISTRY);
        this.name = null;
        this.games = new ArrayList<>();
    }
    
    public Publisher(String name) {
        //super(IdRegistry.PUBLISHERIDREGISTRY);
        this.name = name;
        this.games = new ArrayList<>();
    }

    public Publisher(String name, List<Game> games) {
        //super(IdRegistry.PUBLISHERIDREGISTRY);
        this.name = name;
        this.games = games;
    }
    
    public Publisher(long id, String name) {
        super(id);
        this.name = name;
        this.games = new ArrayList<>();
    }
        
    public Publisher(long id, String name, List<Game> games) {
        super(id);
        this.name = name;
        this.games = games;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }
    
    public void addGame(Game game) {
        games.add(game);
    }
    
    public void addGames(Collection<Game> games) {
        this.games.addAll(games);
    }
    
    public void removeGame(Game game) {
        games.remove(game);
    }
    
    public void removeGames(Collection<Game> games) {
        this.games.removeAll(games);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        
        if(! obj.getClass().equals(this.getClass()))
            return false;
        
        Publisher other = (Publisher) obj;
        
        return this.getId() == other.getId()
                && this.getName().equals(other.getName());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.id);
        hash = 17 * hash + Objects.hashCode(this.name);
        //Causes a stack overflow, obviously...
        //hash = 17 * hash + Objects.hashCode(this.games);
        return hash;
    }
    
}
