/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exevan.ipdomain.domain.util;

import com.exevan.ipdomain.domain.Game;
import java.util.Comparator;

/**
 *
 * @author Exevan
 */
public class GameDateComparator implements Comparator<Game> {

    @Override
    public int compare(Game game1, Game game2) {
        if (game1.getReleaseDate().before(game2.getReleaseDate()))
            return -1;
        else if (game1.getReleaseDate().after(game2.getReleaseDate()))
            return 1;
        else
            return 0;
    }
        
}
