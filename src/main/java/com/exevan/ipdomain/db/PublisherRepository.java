package com.exevan.ipdomain.db;

import com.exevan.ipdomain.domain.Game;
import com.exevan.ipdomain.domain.Publisher;
import java.util.Collection;

/**
 *
 * @author Exevan
 */
public interface PublisherRepository {

    public Publisher addPublisher(Publisher publisher);

    public Collection<Game> readAllGamesFromPublisher(long id);

    public Publisher readPublisher(long id);

    public Collection<Publisher> readPublishers();

    public Publisher updatePublisher(long id, String name);
    
    public void addGameToPublisher(long id, Game game);
            
    public void removeGameFromPublisher(long id, Game game);       
    
    public void removeAllGamesByPublisher(long id);

    public void removePublisher(long id);
    
}
