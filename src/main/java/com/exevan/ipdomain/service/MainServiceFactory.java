/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exevan.ipdomain.service;

import com.exevan.ipdomain.db.DatabaseRepository;
import com.exevan.ipdomain.db.GameRepository;
import com.exevan.ipdomain.db.LocalRepository;
import com.exevan.ipdomain.db.PublisherRepository;
import com.exevan.ipdomain.domain.Game;
import com.exevan.ipdomain.domain.Genre;
import com.exevan.ipdomain.domain.Publisher;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author Exevan
 */
public class MainServiceFactory {

    private static final MainServiceFactory factory = new MainServiceFactory();

    public static MainServiceFactory createMainServiceFactory() {
        return factory;
    }

    public MainService createEmptyMainService(int mode) {
        GameRepository gameRepository;
        PublisherRepository publisherRepository;

        switch (mode) {
            case LOCAL_MODE: {
                LocalRepository repository = new LocalRepository();
                gameRepository = repository;
                publisherRepository = repository;
            }
            break;
            case DATABASE_MODE: {
                DatabaseRepository repository = new DatabaseRepository("VidyaPU");
                gameRepository = repository;
                publisherRepository = repository;
            }
            break;
            default:
                throw new ServiceException("Error: could not create main service, illegal database type " + mode);
        }
        return new MainService(gameRepository, publisherRepository);
        //return new MainService(gameRepository, publisherRepository);
    }

    public MainService createTestMainService(int mode) {
        MainService service = createEmptyMainService(mode);
        testInit(service);
        return service;
    }

    private MainService testInit(MainService service) {
        Publisher ubi = new Publisher("Ubisoft");
        service.addPublisher(ubi);
        service.addGame(new Game("Farcry 4", Genre.Shooter, getDate(2014, Calendar.NOVEMBER, 18), ubi));
        service.addGame(new Game("Assassins Creed Syndicate", Genre.Adventure, getDate(2015, Calendar.OCTOBER, 23), ubi));
        service.addGame(new Game("Tom Clancy's The Division", Genre.Shooter, getDate(2016, Calendar.MARCH, 8), ubi));

        Publisher ea = new Publisher("EA Games");
        service.addPublisher(ea);
        service.addGame(new Game("SimCity 5", Genre.Simulation, getDate(2013, Calendar.MARCH, 7), ea));
        service.addGame(new Game("Battlefield 4", Genre.Shooter, getDate(2013, Calendar.NOVEMBER, 1), ea));
        service.addGame(new Game("Mass Effect 3", Genre.Adventure, getDate(2012, Calendar.MARCH, 9), ea));
        service.addGame(new Game("Dead Space 3", Genre.Action, getDate(2013, Calendar.FEBRUARY, 8), ea));

        Publisher wild = new Publisher("Studio Wildcard");
        service.addPublisher(wild);
        service.addGame(new Game("Ark Survival Evolved", Genre.Survival, getDate(2016, Calendar.JUNE, 1), wild));
        
        return service;
    }

    private Date getDate(int year, int month, int day) {
        Calendar c = GregorianCalendar.getInstance();
        c.set(year, month, day);
        return c.getTime();
    }

    public static final int LOCAL_MODE = 0;
    public static final int DATABASE_MODE = 1;
}
