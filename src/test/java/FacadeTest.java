/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.exevan.ipdomain.service.MainService;
import com.exevan.ipdomain.domain.Game;
import com.exevan.ipdomain.domain.Genre;
import com.exevan.ipdomain.domain.Publisher;
import com.exevan.ipdomain.domain.util.IdRegistry;
import com.exevan.ipdomain.service.MainServiceFactory;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Exevan
 */
public class FacadeTest extends TestCase {
    
    private final MainServiceFactory mainServiceFactory = MainServiceFactory.createMainServiceFactory();
    private MainService mainService;
    private Publisher ubi, ea, wild;
    private Game ubi1, ubi2, ubi3, ea1, ea2, ea3, ea4, wild1;
    
    
    @Before
    @Override
    public void setUp() {
        buildFacade();        
    }
    
    @After
    @Override
    public void tearDown() {
        destroyFacade();
    }
    
    private void buildFacade() {
        mainService = mainServiceFactory.createEmptyMainService(MainServiceFactory.LOCAL_MODE);
        
        ubi = new Publisher("Ubisoft");
        mainService.addPublisher(ubi);
        mainService.addGame(ubi1 = new Game("Farcry 4", Genre.Shooter, getDate(2014, Calendar.NOVEMBER, 18), ubi));
        mainService.addGame(ubi2 = new Game("Assassins Creed Syndicate", Genre.Adventure, getDate(2015, Calendar.OCTOBER, 23), ubi));
        mainService.addGame(ubi3 = new Game("Tom Clancy's The Division", Genre.Shooter, getDate(2016, Calendar.MARCH, 8), ubi));                
        
        ea = new Publisher("EA Games");
        mainService.addPublisher(ea);
        mainService.addGame(ea1 = new Game("SimCity 5", Genre.Simulation, getDate(2013, Calendar.MARCH, 7), ea));
        mainService.addGame(ea2 = new Game("Battlefield 4", Genre.Shooter, getDate(2013, Calendar.NOVEMBER, 1), ea));
        mainService.addGame(ea3 = new Game("Mass Effect 3", Genre.Adventure, getDate(2012, Calendar.MARCH, 9), ea));
        mainService.addGame(ea4 = new Game("Dead Space 3", Genre.Action, getDate(2013, Calendar.FEBRUARY, 8), ea));       
        
        wild = new Publisher("Studio Wildcard");
        mainService.addPublisher(wild);
        mainService.addGame(wild1 = new Game("Ark Survival Evolved", Genre.Survival, getDate(2016, Calendar.JUNE, 1), wild));       
    }
    
    private Date getDate(int year, int month, int day) {
        Calendar c = GregorianCalendar.getInstance();
        c.set(year, month, day);
        return c.getTime();
    }
    
    private void destroyFacade() {
        mainService = null;
    }
    
    @Test
    public void testAddGameObjectSuccess() {
        Game ea5 = getTestGameEA5();
        
        mainService.addGame(ea5);
        
        assertEquals(ea5, mainService.getGame(ea5.getId()));
        
//        assertEquals(ea5.getTitle(), facade.getGame(ea5.getTitle()).getTitle());
//        assertEquals(ea5.getGenre(), facade.getGame(ea5.getTitle()).getGenre());
//        assertEquals(ea5.getReleaseDate(), facade.getGame(ea5.getTitle()).getReleaseDate());
//        assertEquals(ea5.getPublisher(), facade.getGame(ea5.getTitle()).getPublisher()); 
    }
    
    @Test
    public void testAddGamesSuccess() {
        List<Game> newGames = new ArrayList<>();
        newGames.add(getTestGameEA5());
        newGames.add(getTestGameEA6());
     
        mainService.addGames(newGames);
 
        assertTrue(mainService.getAllGames().containsAll(newGames));
    }

    
    @Test
    public void testAddPublisherObjectSuccess() {
        Publisher para = getTestPublisher1();
        
        mainService.addPublisher(para);
        
        assertEquals(para, mainService.getPublisher(para.getId()));
        
        //assertEquals(para.getName(), facade.getPublisher(para.getName()).getName());
    }
    
    @Test
    public void testAddPublishersSuccess() {
        List<Publisher> newPublishers = new ArrayList<>();
        newPublishers.add(getTestPublisher1());
        newPublishers.add(getTestPublisher2());
        
        mainService.addPublishers(newPublishers);
        
        assertTrue(mainService.getAllPublishers().containsAll(newPublishers));       
    }
    
    @Test
    public void testGetAllGamesSuccess() {
        List<Game> allGames = new ArrayList<>();
        allGames.add(ubi1);
        allGames.add(ubi2);
        allGames.add(ubi3);
        allGames.add(ea1);
        allGames.add(ea2);
        allGames.add(ea3);
        allGames.add(ea4);
        allGames.add(wild1);
        
        Collection<Game> games = mainService.getAllGames();
        
        assertTrue(containSame(games, allGames));        
    }
    
    @Test
    public void testGetAllPublishersSuccess() {
        List<Publisher> allPublishers = new ArrayList<>();
        allPublishers.add(ubi);
        allPublishers.add(ea);
        allPublishers.add(wild);
        
        Collection<Publisher> publishers = mainService.getAllPublishers();
        
        assertTrue(containSame(publishers, allPublishers));      
    }
    
    @Test
    public void testGetGameSuccess() {
        assertEquals(ubi1, mainService.getGame(ubi1.getId()));
        assertEquals(ea1, mainService.getGame(ea1.getId()));
        assertEquals(wild1, mainService.getGame(wild1.getId()));
        
    }
    
    @Test
    public void testGetNewestGamesSuccess() {
        Collection<Game> newestGames = mainService.getNewestGames(1);
        assertTrue(newestGames.contains(wild1));
    }
    
    @Test
    public void testGetPublisherSuccess() {
        assertEquals(ubi, mainService.getPublisher(ubi.getId()));
        assertEquals(ea, mainService.getPublisher(ea.getId()));
        assertEquals(wild, mainService.getPublisher(wild.getId()));
    }
    
    @Test
    public void testRemoveGameSuccess() {
        mainService.removeGame(wild1.getId());
        
        assertTrue(! mainService.getAllGames().contains(wild1));
    }
    
    @Test
    public void testRemovePublisherSuccess() {
        mainService.removePublisher(wild.getId());
        
        assertTrue(! mainService.getAllPublishers().contains(wild));
    }
    
    private Game getTestGameEA5() {
        return new Game("Crysis 3", Genre.Shooter, getDate(2013, Calendar.FEBRUARY, 21), ea);
    }
    
    private Game getTestGameEA6() {
        return new Game("The Sims 3", Genre.Simulation, getDate(2009, Calendar.JUNE, 2), ea);
    }
    
    private Publisher getTestPublisher1() {
        return new Publisher("Paradox Interactive");
    }
    
    private Publisher getTestPublisher2() {
        return new Publisher("Mojang");
    }
    
    private <T> boolean containSame(Collection<T> c1, Collection<T> c2) {        
        for (T t : c1) {
            if (! c2.contains(t))
                return false;
        }
        for (T t : c2) {
            if(! c1.contains(t))
                return false;
        }
        return true;
    }
}