package com.exevan.ipdomain.domain;

import javax.persistence.*;

/**
 *
 * @author Exevan
 */
@MappedSuperclass
public class Entity {
    
    @Id
    @GeneratedValue
    protected long id;
    
    /**
    public Entity(IdRegistry idRegistry) {
        this.id = idRegistry.getId();
    }
    **/
    
    public Entity() {
        //this.id = -1;
    }
    
    public Entity(long id) {
        this.id = id;
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
   
}
