/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exevan.ipdomain.db;

import com.exevan.ipdomain.domain.Game;
import com.exevan.ipdomain.domain.Genre;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author Wouter Dumoulin
 */
public interface GameRepository {
    
    public Collection<Game> readGames();
    
    public Game readGame(long id);
    
    public Game addGame(Game game);
    
    public Game updateGame(long id, String title, Genre genre, Date releaseDate, long publisherId);
    
    public void removeGame(long id);
        
}
