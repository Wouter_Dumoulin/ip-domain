/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exevan.ipdomain.db;

/**
 *
 * @author Exevan
 */
public class DatabaseException extends RuntimeException {
    
    public DatabaseException(Throwable cause) {
        super(cause);
    }
    
}
