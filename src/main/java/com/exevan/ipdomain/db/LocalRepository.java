/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exevan.ipdomain.db;

import com.exevan.ipdomain.domain.Game;
import com.exevan.ipdomain.domain.Genre;
import com.exevan.ipdomain.domain.Publisher;
import com.exevan.ipdomain.domain.util.IdRegistry;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author Wouter Dumoulin
 */
public class LocalRepository implements GameRepository, PublisherRepository {

    private final Map<Long, Game> games;
    private final Map<Long, Publisher> publishers;

    public LocalRepository() {
        this.games = new HashMap<>();
        this.publishers = new HashMap<>();
    }

    @Override
    public Collection<Game> readGames() {
        List<Game> allGames = new ArrayList<>();
        for (Game game : games.values()) {
            allGames.add(game);
        }
        return allGames;
    }

    @Override
    public Collection<Publisher> readPublishers() {
        return publishers.values();
    }

    @Override
    public Collection<Game> readAllGamesFromPublisher(long id) {
        return publishers.get(id).getGames();
    }

    @Override
    public Game readGame(long id) {
        return games.get(id);
    }

    @Override
    public Publisher readPublisher(long id) {
        return publishers.get(id);
    }

    @Override
    public Game addGame(Game game) {
        game.setId(IdRegistry.GAMEIDREGISTRY.getId());
        games.put(game.getId(), game);
        return this.readGame(game.getId());
    }

    @Override
    public Publisher addPublisher(Publisher publisher) {
        publisher.setId(IdRegistry.PUBLISHERIDREGISTRY.getId());
        publishers.put(publisher.getId(), publisher);
        return this.readPublisher(publisher.getId());
    }

    @Override
    public Game updateGame(long id, String title, Genre genre, Date releaseDate, long publisherId) {
        Game game = readGame(id);
        game.setTitle(title);
        game.setGenre(genre);
        game.setReleaseDate(releaseDate);
        removeGameFromPublisher(game.getPublisher().getId(), game);
        addGameToPublisher(publisherId, game);
        game.setPublisher(readPublisher(publisherId));
        return this.readGame(id);
    }

    @Override
    public Publisher updatePublisher(long id, String name) {
        Publisher publisher = readPublisher(id);
        publisher.setName(name);
        return this.readPublisher(id);
    }

    @Override
    public void addGameToPublisher(long id, Game game) {
        Publisher publisher = readPublisher(id);
        publisher.addGame(game);
    }

    @Override
    public void removeGameFromPublisher(long id, Game game) {
        Publisher publisher = readPublisher(id);
        publisher.removeGame(game);
    }

    @Override
    public void removeGame(long id) {
        games.remove(id);
        IdRegistry.GAMEIDREGISTRY.unregisterId(id);
    }

    @Override
    public void removePublisher(long id) {
        removeAllGamesByPublisher(id);
        publishers.remove(id);
        IdRegistry.PUBLISHERIDREGISTRY.unregisterId(id);
    }

    @Override
    public void removeAllGamesByPublisher(long id) {
        List<Game> gamesFromPublisher = this.publishers.get(id).getGames();
        for (Game game : gamesFromPublisher) {
            this.games.remove(game.getId());
        }
        this.publishers.get(id).getGames().clear();
    }
}
