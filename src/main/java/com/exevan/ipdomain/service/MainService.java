package com.exevan.ipdomain.service;


import com.exevan.ipdomain.domain.Game;
import com.exevan.ipdomain.domain.Genre;
import com.exevan.ipdomain.domain.Publisher;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import com.exevan.ipdomain.db.GameRepository;
import com.exevan.ipdomain.db.PublisherRepository;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Wouter Dumoulin
 */
public class MainService implements GameService, PublisherService {
    
    private final GameRepository gameRepository;
    private final PublisherRepository publisherRepository;
    
    
    public MainService(GameRepository gameRepository, PublisherRepository publisherRepository) {
        this.gameRepository = gameRepository;
        this.publisherRepository = publisherRepository;
    }
    
    @Override
    public Collection<Game> getAllGames() {
        return gameRepository.readGames();
    }
    
    @Override
    public Collection<Publisher> getAllPublishers() {
        return publisherRepository.readPublishers();
    }
    
    @Override
    public Game getGame(long id) {
        return gameRepository.readGame(id);
    }
    
    @Override
    public Publisher getPublisher(long id) {
        return publisherRepository.readPublisher(id);
    }
    
    @Override
    public Collection<Game> getNewestGames(int amount) {
        List<Game> allGames = (List<Game>) getAllGames();
        List<Game> games = new ArrayList<>();
        Collections.sort(allGames, Game.GAMEDATECOMPARATOR);
        for (int i = 0; i < amount; i++) {
            games.add(allGames.get(allGames.size() - i - 1));
        }
        return games;
    }

    
    @Override
    public Game addGame(Game game) {
        game.getPublisher().addGame(game);
        return gameRepository.addGame(game);
    }
    
        
    @Override
    public void addGames(Collection<Game> games) {
        for (Game game : games) {
            addGame(game);
        }
    }
        
    @Override
    public Publisher addPublisher(Publisher publisher) {
        return publisherRepository.addPublisher(publisher);
    }
    
    @Override
    public void addPublishers(Collection<Publisher> publishers) {
        for (Publisher publisher : publishers) {
            addPublisher(publisher);
        }
    }

    @Override
    public Game updateGame(long id, String title, Genre genre, Date releaseDate, long publisherId) {
        Game game = getGame(id);
        //remove game from old publisher
        publisherRepository.removeGameFromPublisher(game.getPublisher().getId(), game);
        //add game to new publisher
        publisherRepository.addGameToPublisher(publisherId, game);
        //update game
        return gameRepository.updateGame(id, title, genre, releaseDate, publisherId);
    }

    @Override
    public void updatePublisher(long id, String name) {
        Publisher publisher = getPublisher(id);
        publisher.setName(name);
    }

    @Override
    public void removeGame(long id) {
        Game game = getGame(id);
        game.getPublisher().removeGame(game);
        gameRepository.removeGame(id);
    }

    @Override
    public void removePublisher(long id) {
        publisherRepository.removeAllGamesByPublisher(id);
        publisherRepository.removePublisher(id);
    }

    @Override
    public Collection<Genre> getAllGenres() {
        return Arrays.asList(Genre.values());
    }
}
