package com.exevan.ipdomain.domain;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Wouter Dumoulin
 */
public enum Genre {
    
    Action, Adventure, Racing, Roleplay, Shooter, Simulation, Strategy, Survival, Undefined;
       
}
