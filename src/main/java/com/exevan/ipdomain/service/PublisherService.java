/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exevan.ipdomain.service;

import com.exevan.ipdomain.domain.Publisher;
import java.util.Collection;

/**
 *
 * @author Exevan
 */
public interface PublisherService {

    Publisher addPublisher(Publisher publisher);

    void addPublishers(Collection<Publisher> publishers);

    Collection<Publisher> getAllPublishers();

    Publisher getPublisher(long id);
    
    void updatePublisher(long id, String name);

    void removePublisher(long id);
    
}
