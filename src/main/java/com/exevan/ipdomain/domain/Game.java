package com.exevan.ipdomain.domain;

import com.exevan.ipdomain.domain.util.GameDateComparator;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Wouter Dumoulin
 */
@javax.persistence.Entity
public class Game extends Entity implements Serializable {
    
    private String title;
    private Genre genre;
    @Temporal(TemporalType.DATE)
    private Date releaseDate;
    @ManyToOne
    private Publisher publisher;

    public Game() {
        //super(IdRegistry.GAMEIDREGISTRY);
        this.title = null;
        this.genre = null;
        this.releaseDate = null;
        this.publisher = null;
    }
    
    public Game(String title, Genre genre, Date releaseDate, Publisher publisher) {
        //super(IdRegistry.GAMEIDREGISTRY);
        this.title = title;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.publisher = publisher;
    }

    public Game(long id, String title, Genre genre, Date releaseDate, Publisher publisher) {
        super(id);
        this.title = title;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.publisher = publisher;
    }
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!obj.getClass().equals(this.getClass())) {
            return false;
        }

        Game other = (Game) obj;

        return  this.getId() == other.getId()
                && this.getTitle().equals(other.getTitle())
                && this.getGenre().equals(other.getGenre())
                && this.getReleaseDate().equals(other.getReleaseDate())
                && this.getPublisher().equals(other.getPublisher());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.id);
        hash = 23 * hash + Objects.hashCode(this.title);
        hash = 23 * hash + Objects.hashCode(this.genre);
        hash = 23 * hash + Objects.hashCode(this.releaseDate);
        hash = 23 * hash + Objects.hashCode(this.publisher);
        return hash;
    }

    public static final GameDateComparator GAMEDATECOMPARATOR = new GameDateComparator();
}
