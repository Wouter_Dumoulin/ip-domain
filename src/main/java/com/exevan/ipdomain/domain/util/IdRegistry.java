/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exevan.ipdomain.domain.util;

import com.exevan.ipdomain.domain.DomainException;

/**
 *
 * @author Exevan
 */
public class IdRegistry {
    
    private final boolean[] ids;

    public IdRegistry() {
        ids = new boolean[MAX_ID+1];
    }
    
    public long getId() {
        for (int i = 0; i < ids.length; i++) {
            if (! ids[i]) {
                ids[i] = true;
                return i;
            }
        }       
        throw new DomainException("Error: could not register id, no more id's available");
    }
    
    public void unregisterId(long id) {
        if (id >= 0 || id <= MAX_ID) {
            ids[(int) id] = false;
        } else
            throw new DomainException("Error: could not unregister id, not between 0 and " + MAX_ID);
    }
    
    public static final IdRegistry GAMEIDREGISTRY = new IdRegistry();
    public static final IdRegistry PUBLISHERIDREGISTRY = new IdRegistry();
    
    public static final int MAX_ID = 65535;
}
