/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exevan.ipdomain.service;

import com.exevan.ipdomain.domain.Game;
import com.exevan.ipdomain.domain.Genre;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author Exevan
 */
public interface GameService {

    Game addGame(Game game);

    void addGames(Collection<Game> games);

    Collection<Game> getAllGames();

    Game getGame(long id);

    Collection<Game> getNewestGames(int amount);
    
    Game updateGame(long id, String title, Genre genre, Date releaseDate, long publisherId);

    void removeGame(long id);
    
    Collection<Genre> getAllGenres();
    
}
