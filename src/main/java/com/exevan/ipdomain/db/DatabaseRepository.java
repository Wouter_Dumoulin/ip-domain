/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exevan.ipdomain.db;

import com.exevan.ipdomain.domain.Game;
import com.exevan.ipdomain.domain.Genre;
import com.exevan.ipdomain.domain.Publisher;
import com.exevan.ipdomain.domain.util.IdRegistry;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author Exevan
 */
public class DatabaseRepository implements GameRepository, PublisherRepository {

    private final EntityManagerFactory factory;
    private final EntityManager manager;

    public DatabaseRepository(String name) {
        factory = Persistence.createEntityManagerFactory(name);
        manager = factory.createEntityManager();
    }

    public void close() {
        try {
            manager.close();
            factory.close();
        } catch (Exception e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public Collection<Game> readGames() {
        try {
            return manager.createQuery("select g from Game g").getResultList();
        } catch (Exception e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public Game readGame(long id) {
        try {
            return (Game) manager.createQuery("select g from Game g where g.id = :id").setParameter("id", id).getSingleResult();
        } catch (Exception e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public Game addGame(Game game) {
        //game.setId(IdRegistry.GAMEIDREGISTRY.getId());
        try {
            manager.getTransaction().begin();
            manager.persist(game);
            manager.flush();
            manager.getTransaction().commit();
            return game;
        } catch (Exception e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public Game updateGame(long id, String title, Genre genre, Date releaseDate, long publisherId) {
        try {
            Game game = readGame(id);
            game.setTitle(title);
            game.setGenre(genre);
            game.setReleaseDate(releaseDate);
            game.setPublisher(readPublisher(publisherId));
            manager.getTransaction().begin();
            manager.merge(game);
            manager.flush();
            manager.getTransaction().commit();
            return game;
        } catch (Exception e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public void removeGame(long id) {
        try {
            Game game = readGame(id);
            manager.getTransaction().begin();
            manager.remove(game);
            manager.flush();
            manager.getTransaction().commit();
        } catch (Exception e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public Publisher addPublisher(Publisher publisher) {
        //publisher.setId(IdRegistry.PUBLISHERIDREGISTRY.getId());
        try {
            manager.getTransaction().begin();
            manager.persist(publisher);
            manager.flush();
            manager.getTransaction().commit();
            return publisher;
        } catch (Exception e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public Collection<Game> readAllGamesFromPublisher(long id) {
        try {
            Publisher publisher = readPublisher(id);
            return publisher.getGames();
        } catch (Exception e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public Collection<Publisher> readPublishers() {
        try {
            return manager.createQuery("select p from Publisher p").getResultList();
        } catch (Exception e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public Publisher readPublisher(long id) {
        try {
            return (Publisher) manager.createQuery("select p from Publisher p where p.id = :id").setParameter("id", id).getSingleResult();
        } catch (Exception e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public Publisher updatePublisher(long id, String name) {
        try {
            Publisher publisher = readPublisher(id);
            publisher.setName(name);
            manager.getTransaction().begin();
            manager.merge(publisher);
            manager.flush();
            manager.getTransaction().commit();
            return publisher;
        } catch (Exception e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public void addGameToPublisher(long id, Game game) {
        try {
            Publisher publisher = readPublisher(id);
            publisher.addGame(game);
            manager.getTransaction().begin();
            manager.merge(publisher);
            manager.flush();
            manager.getTransaction().commit();
        } catch (Exception e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public void removeGameFromPublisher(long id, Game game) {
        try {
            Publisher publisher = readPublisher(id);
            publisher.removeGame(game);
            manager.getTransaction().begin();
            manager.merge(publisher);
            manager.flush();
            manager.getTransaction().commit();
        } catch (Exception e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public void removeAllGamesByPublisher(long id) {
        try {
            Publisher publisher = readPublisher(id);
            manager.getTransaction().begin();
            for (Game game : publisher.getGames()) {
                manager.remove(game);
            }
            manager.flush();
            manager.getTransaction().commit();
        } catch (Exception e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public void removePublisher(long id) {
        try {
            Publisher publisher = readPublisher(id);
            removeAllGamesByPublisher(id);
            manager.getTransaction().begin();
            manager.remove(publisher);
            manager.flush();
            manager.getTransaction().commit();
        } catch (Exception e) {
            throw new DatabaseException(e);
        }
    }

}
